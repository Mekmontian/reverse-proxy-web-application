# Reverse Proxy Web Application

make web application using reverse proxy in order to make frontend and backend url endpoint the same

## Dependency
1. Vue.js (frontend lightweight framework)
2. Golang (backend with echo api framework)

## Objective
1. Make reverse proxy with `Apache`
2. Make reverse proxy with `Nginx`


## Intregation
using `Docker` for make container service and `Docker Compose` as container composer