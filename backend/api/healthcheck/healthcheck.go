package healthcheck

import (
	"net/http"
	"reverse-proxy-backend/api/model"

	"github.com/labstack/echo/v4"
)

// Interface : health check interface for check server beat
type Interface interface {
	Initial(e *echo.Group)
}

type healthCheckRoute struct {
}

// NewHealthCheckRoute : create health check route module
func NewHealthCheckRoute() Interface {
	return &healthCheckRoute{}
}

func (r *healthCheckRoute) Initial(e *echo.Group) {
	api := e.Group("/health")
	api.GET("", healthCheck)
}

func healthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, model.BaseResponse{
		Msg: "OK",
	})
}
