package main

import (
	"net/http"
	"reverse-proxy-backend/api/healthcheck"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	logger = middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format:           "${time_custom} method=${method}, uri=${uri}, status=${status}, origin=${header:origin}\n",
		CustomTimeFormat: "2006-01-02 15:04:05",
	})
	cors = middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{
			"*",
		},
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderContentType,
		},
		AllowMethods: []string{
			echo.GET,
			echo.OPTIONS,
		},
	})
)

func main() {
	e := echo.New()
	e.Use(logger)
	e.Use(cors)

	// Route
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	apiGroup := e.Group("/api")
	healthcheck.NewHealthCheckRoute().Initial(apiGroup)

	e.Logger.Fatal(e.Start(":1323"))
}
