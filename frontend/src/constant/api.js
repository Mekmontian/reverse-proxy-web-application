import axios from 'axios';

const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:1323' : '';

const api = axios.create({
  baseURL,
});

export default api;
