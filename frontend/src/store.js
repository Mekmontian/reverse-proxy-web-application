import Vue from 'vue';
import Vuex from 'vuex';
import API from '@/constant/api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: '',
  },
  mutations: {
    setStatus: (state, payload) => { state.status = payload; },
  },
  actions: {
    fetchHealthCheckAPI: ({ commit }) => {
      API.get('/api/health').then(res => commit('setStatus', res.data.message)).catch(err => console.log(err));
    },
  },
  getters: {
    getStatus: state => state.status,
  },
});
